import matplotlib.pyplot as plt
import numpy as np
from SGWT import g, h_c

x = np.linspace(0, 1, 500)

# Figure: Visualisation of the omega and g_c functions
plt.plot(x, g(x), '-',label=r'$\omega(x)$')
plt.plot(x, h_c(x, c=1), '--', label=r'$h_c(x)$')

plt.xlim((0, 1))
plt.xlabel(r'$x$')
plt.ylabel(r'$\omega(x), h_c(x)$')
plt.legend()
plt.grid(ls=':')
plt.tight_layout()
plt.show()