from texttable import Texttable
import pandas as pd
import latextable

snr_in_sigma = pd.read_csv("data/mesh/snr_in_sigma.csv").round(2).astype(str)
snr_out_sigma_sgwt_mse = pd.read_csv("data/mesh/snr_out_sigma_sgwt_mse.csv").round(2).astype(str)
snr_out_sigma_sgwtcj_suremc = pd.read_csv("data/mesh/snr_out_sigma_sgwtcj_suremc.csv").round(2).astype(str)

# Table
table = pd.DataFrame(
    {
        "sigma": snr_in_sigma["sigma"],
        "SNR_in": 
            snr_in_sigma["snr_in_mean"] + " $\pm$ " + 
            snr_in_sigma["snr_in_std"],
        "SGWT (MSE)": 
            snr_out_sigma_sgwt_mse["snr_out_mean"] + " $\pm$ " + 
            snr_out_sigma_sgwt_mse["snr_out_std"],
        "SGWT CJ (SURE MC)": 
            snr_out_sigma_sgwtcj_suremc["snr_out_mean"] + " $\pm$ " + 
            snr_out_sigma_sgwtcj_suremc["snr_out_std"]
    }
)

table = table.T
rows = table.reset_index().to_numpy()

# Latex
table_latex = Texttable()
table_latex.set_precision(2)
table_latex.set_cols_align(["l"] + ["c"]*table.shape[1])
table_latex.set_deco(False)
table_latex.add_rows(rows, header=False)

print(table_latex.draw())
print(latextable.draw_latex(table_latex))