from sklearn.decomposition import TruncatedSVD
from scipy import sparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import SGWT
import time

with np.load('data/NY/pickups.npz') as data:
    f = data['pickups']

## Denoising
L_sparse = sparse.load_npz('data/NY/L.npz')
b = 2
beta = 2
policy = "uniform"
thresh = "percentiles" # threshold set in abs(W_f)

# Complete SGWT
evalues, evectors = np.linalg.eigh(L_sparse.toarray())
J = int(np.floor(np.log(max(evalues))/np.log(b))) + 2
tf = SGWT.frame(evalues, evectors, b=b)
W = np.array(tf)
diagWWt = (W**2).sum(axis=1) # = np.diag(W@W.T)
W_f_scales = np.split(W@f, J+1)

# SGWT polynomial approximation
svd = TruncatedSVD(n_components=1, n_iter=50, random_state=0)
svd.fit(L_sparse)
lambda_max = svd.singular_values_[0] # maximum eigenvalue estimate
m = 100 # Chebyshev polynomial approximation degree
cc = SGWT.tight_sgwt_cheby(amin=0, amax=lambda_max, m=m, lmax=lambda_max, b=b)
ccj = SGWT.tight_sgwt_cheby_jackson(amin=0, amax=lambda_max, m=m, lmax=lambda_max, b=b)
hat_diagWWt = SGWT.hat_diagWWt(L_sparse, lambda_max, cc, K=10)

tilde_f_cgm_eps = np.load("data/NY/pickups_cgm_eps.npz")
tilde_f_agm_eps = np.load("data/NY/pickups_agm_eps.npz")
snr_in_eps = pd.read_csv("data/NY/snr_in_eps.csv")

snr_out_cgm_mean_sgwt_mse = []
snr_out_cgm_mean_sgwt_sure = []
snr_out_cgm_mean_sgwtcj_suremc = []
snr_out_cgm_std_sgwt_mse = []
snr_out_cgm_std_sgwt_sure = []
snr_out_cgm_std_sgwtcj_suremc = []
snr_out_agm_mean_sgwt_mse = []
snr_out_agm_mean_sgwt_sure = []
snr_out_agm_mean_sgwtcj_suremc = []
snr_out_agm_std_sgwt_mse = []
snr_out_agm_std_sgwt_sure = []
snr_out_agm_std_sgwtcj_suremc = []

start_time = time.time()
for eps_id, eps in enumerate(tilde_f_cgm_eps.files):
    print(str(eps_id+1) + "/" + str(snr_in_eps.shape[0]))
    snr_out_cgm_sgwt_mse = []
    snr_out_cgm_sgwt_sure = []
    snr_out_cgm_sgwtcj_suremc = []
    snr_out_agm_sgwt_mse = []
    snr_out_agm_sgwt_sure = []
    snr_out_agm_sgwtcj_suremc = []
    
    tilde_f_cgm = tilde_f_cgm_eps[eps]
    tilde_f_agm = tilde_f_agm_eps[eps]

    for i in range(tilde_f_cgm.shape[1]):
        ## Classical Gaussian mechanism
        tilde_f = tilde_f_cgm[:, i]
        
        # SGWT (MSE)
        W_tilde_f_scales = np.split(W@tilde_f, J+1)
        W_hat_f_scales = SGWT.ld_MSE_threshold(W_tilde_f_scales, W_f_scales, beta, thresh)
        hat_f_cgm_sgwt_mse = W.T@np.concatenate(W_hat_f_scales)
        
        # SGWT (SURE)
        sigma = snr_in_eps.loc[eps_id, 'sigma_cgm']
        W_hat_f_scales = SGWT.ld_SURE_threshold(W_tilde_f_scales, diagWWt, beta, sigma, policy, thresh)
        hat_f_cgm_sgwt_sure = W.T@np.concatenate(W_hat_f_scales)
        
        # SGWT Chebyshev-Jackson (SURE Monte Carlo)
        W_tilde_f_scales = SGWT.sgwt_cheby_sparse(tilde_f, L_sparse, ccj, lambda_max, lmin=0)
        W_hat_f_scales = SGWT.ld_SURE_threshold(W_tilde_f_scales, hat_diagWWt, beta, sigma, policy, thresh)
        hat_f_cgm_sgwtcj_suremc = SGWT.inv_tf_cheby_sparse(W_hat_f_scales, L_sparse, ccj, lambda_max, lmin=0)
        
        ## Analytic Gaussian mechanism
        tilde_f = tilde_f_agm[:, i]
        
        # SGWT (MSE)
        W_tilde_f_scales = np.split(W@tilde_f, J+1)
        W_hat_f_scales = SGWT.ld_MSE_threshold(W_tilde_f_scales, W_f_scales, beta, thresh)
        hat_f_agm_sgwt_mse = W.T@np.concatenate(W_hat_f_scales)
        
        # SGWT (SURE)
        sigma = snr_in_eps.loc[eps_id, 'sigma_agm']
        W_hat_f_scales = SGWT.ld_SURE_threshold(W_tilde_f_scales, diagWWt, beta, sigma, policy, thresh)
        hat_f_agm_sgwt_sure = W.T@np.concatenate(W_hat_f_scales)
        
        # SGWT Chebyshev-Jackson (SURE Monte Carlo)
        W_tilde_f_scales = SGWT.sgwt_cheby_sparse(tilde_f, L_sparse, ccj, lambda_max, lmin=0)
        W_hat_f_scales = SGWT.ld_SURE_threshold(W_tilde_f_scales, hat_diagWWt, beta, sigma, policy, thresh)
        hat_f_agm_sgwtcj_suremc = SGWT.inv_tf_cheby_sparse(W_hat_f_scales, L_sparse, ccj, lambda_max, lmin=0)

        snr_out_cgm_sgwt_mse.append(SGWT.snr(f, hat_f_cgm_sgwt_mse))
        snr_out_cgm_sgwt_sure.append(SGWT.snr(f, hat_f_cgm_sgwt_sure))
        snr_out_cgm_sgwtcj_suremc.append(SGWT.snr(f, hat_f_cgm_sgwtcj_suremc))
        snr_out_agm_sgwt_mse.append(SGWT.snr(f, hat_f_agm_sgwt_mse))
        snr_out_agm_sgwt_sure.append(SGWT.snr(f, hat_f_agm_sgwt_sure))
        snr_out_agm_sgwtcj_suremc.append(SGWT.snr(f, hat_f_agm_sgwtcj_suremc))
        
    snr_out_cgm_mean_sgwt_mse.append(np.mean(snr_out_cgm_sgwt_mse))
    snr_out_cgm_mean_sgwt_sure.append(np.mean(snr_out_cgm_sgwt_sure))
    snr_out_cgm_mean_sgwtcj_suremc.append(np.mean(snr_out_cgm_sgwtcj_suremc))
    snr_out_cgm_std_sgwt_mse.append(np.std(snr_out_cgm_sgwt_mse))
    snr_out_cgm_std_sgwt_sure.append(np.std(snr_out_cgm_sgwt_sure))
    snr_out_cgm_std_sgwtcj_suremc.append(np.std(snr_out_cgm_sgwtcj_suremc))
    snr_out_agm_mean_sgwt_mse.append(np.mean(snr_out_agm_sgwt_mse))
    snr_out_agm_mean_sgwt_sure.append(np.mean(snr_out_agm_sgwt_sure))
    snr_out_agm_mean_sgwtcj_suremc.append(np.mean(snr_out_agm_sgwtcj_suremc))
    snr_out_agm_std_sgwt_mse.append(np.std(snr_out_agm_sgwt_mse))
    snr_out_agm_std_sgwt_sure.append(np.std(snr_out_agm_sgwt_sure))
    snr_out_agm_std_sgwtcj_suremc.append(np.std(snr_out_agm_sgwtcj_suremc))

snr_out_eps_sgwt_mse = pd.DataFrame({
    'snr_out_cgm_mean': snr_out_cgm_mean_sgwt_mse,
    'snr_out_agm_mean': snr_out_agm_mean_sgwt_mse,
    'snr_out_cgm_std': snr_out_cgm_std_sgwt_mse,
    'snr_out_agm_std': snr_out_agm_std_sgwt_mse
})
snr_out_eps_sgwt_sure = pd.DataFrame({
    'snr_out_cgm_mean': snr_out_cgm_mean_sgwt_sure,
    'snr_out_agm_mean': snr_out_agm_mean_sgwt_sure,
    'snr_out_cgm_std': snr_out_cgm_std_sgwt_sure,
    'snr_out_agm_std': snr_out_agm_std_sgwt_sure
})
snr_out_eps_sgwtcj_suremc = pd.DataFrame({
    'snr_out_cgm_mean': snr_out_cgm_mean_sgwtcj_suremc,
    'snr_out_agm_mean': snr_out_agm_mean_sgwtcj_suremc,
    'snr_out_cgm_std': snr_out_cgm_std_sgwtcj_suremc,
    'snr_out_agm_std': snr_out_agm_std_sgwtcj_suremc
})
print("--- %s seconds ---" % round(time.time() - start_time))

snr_out_eps_sgwt_mse.to_csv("data/NY/snr_out_eps_sgwt_mse.csv", index=False)
snr_out_eps_sgwt_sure.to_csv("data/NY/snr_out_eps_sgwt_sure.csv", index=False)
snr_out_eps_sgwtcj_suremc.to_csv("data/NY/snr_out_eps_sgwtcj_suremc.csv", index=False)


## Visualizing results
snr_in_eps = pd.read_csv("data/NY/snr_in_eps.csv")
snr_out_eps_sgwt_mse = pd.read_csv("data/NY/snr_out_eps_sgwt_mse.csv")
snr_out_eps_sgwt_sure = pd.read_csv("data/NY/snr_out_eps_sgwt_sure.csv")
snr_out_eps_sgwtcj_suremc = pd.read_csv("data/NY/snr_out_eps_sgwtcj_suremc.csv")
snr_out_eps_dfsfusedlasso_mse = pd.read_csv("data/NY/snr_out_eps_dfsfusedlasso_mse.csv")
snr_out_eps_dfsfusedlasso_sure = pd.read_csv("data/NY/snr_out_eps_dfsfusedlasso_sure.csv")

plt.plot(snr_in_eps['epsilon'], snr_in_eps['snr_in_cgm_mean'], 'k-', label='cGM - Input noise')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_dfsfusedlasso_mse['snr_out_cgm_mean'], 'ro-', label='cGM - DFS fused lasso (MSE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_dfsfusedlasso_sure['snr_out_cgm_mean'], 'yP-', label='cGM - DFS fused lasso (SURE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_sgwt_mse['snr_out_cgm_mean'], 'ms-', label='cGM - SGWT (MSE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_sgwt_sure['snr_out_cgm_mean'], 'cX-', label='cGM - SGWT (SURE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_sgwtcj_suremc['snr_out_cgm_mean'], 'b^-', label='cGM - SGWT CJ (SURE MC)')

plt.plot(snr_in_eps['epsilon'], snr_in_eps['snr_in_agm_mean'], 'k:', label='aGM - Input noise')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_dfsfusedlasso_mse['snr_out_agm_mean'], 'ro:', label='aGM - DFS fused lasso (MSE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_dfsfusedlasso_sure['snr_out_agm_mean'], 'yP:', label='aGM - DFS fused lasso (SURE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_sgwt_mse['snr_out_agm_mean'], 'ms:', label='aGM - SGWT (MSE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_sgwt_sure['snr_out_agm_mean'], 'cX:', label='aGM - SGWT (SURE)')
plt.plot(snr_in_eps['epsilon'], snr_out_eps_sgwtcj_suremc['snr_out_agm_mean'], 'b^:', label='aGM - SGWT CJ (SURE MC)')

plt.xlim((snr_in_eps['epsilon'].min(), snr_in_eps['epsilon'].max()))
plt.xlabel(r'$\epsilon$')
plt.ylabel('SNR (dB)')
plt.legend()
plt.grid(ls=':')
plt.show()
