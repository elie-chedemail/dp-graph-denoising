from scipy import sparse
import numpy as np

## Complete SGWT
# omega function
def g(x, b=2):
    mid = (x >= 1/b) & (x <= 1)

    res = np.ones_like(x)
    res[mid] = b*x[mid]/(1-b)+b/(b-1)
    res[x>1] = 0
    
    return res

def f(x):
    res = np.zeros_like(x)
    res[x>0] = np.exp(-1/x[x>0])
    
    return res

def g_c(x, c):
    return f(x)/(f(x) + f(c-x))

# alternative omega function
def h_c(x, c):
    res = np.ones_like(x)
    res[x>=0] = g_c(x[x>=0]+1, c) * g_c(1-x[x>=0], c)
    return res

# psi function
def zeta(x, k, b=2):
    if k==0:
        return g(x,b)
    else:
        return g(b**(-k)*x)-g(b**(-k+1)*x)

# SGWT frame
def frame(evalues, evectors, b=2):
    lmax = max(evalues)
    J = int(np.floor(np.log(lmax)/np.log(b))) + 2
    N = len(evalues)
    r = np.zeros((N, N, J+1))
    for k in range(J+1):
        print("step %d/%d in frame" % (k, J))
        G = np.diag(np.sqrt(zeta(evalues, k, b)))
        W = evectors@G@evectors.T
        r[:, :, k] = W
        
    r = r.reshape((N, N*(J+1)), order='F')
    tf = list(r.transpose())

    return tf


## SGWT polynomial approximation
# Chebyshev coefficients
def tight_sgwt_cheby(amin, amax, m, lmax, b=2):
    J = int(np.floor(np.log(lmax)/np.log(b))) + 2
    N = m + 1
    a1 = (amax - amin)/2
    a2 = (amax + amin)/2
    l = np.array(range(N))
    cl = []
    for k in range(J + 1):
        f = np.sqrt(zeta(a1 * np.cos(np.pi * (l + 0.5)/N) + a2, k, b))
        K = (l + 0.5)
        cl.append(2/N * np.matmul(f, np.cos(np.pi/N * np.outer(K, l))))
        
    return cl

# Chebyshev-Jackson coefficients
def tight_sgwt_cheby_jackson(amin, amax, m, lmax, b=2):
    J = int(np.floor(np.log(lmax)/np.log(b))) + 2
    N = m + 1
    a1 = (amax - amin)/2
    a2 = (amax + amin)/2
    l = np.array(range(N))
    cl = []
    for k in range(J + 1):
        f = np.sqrt(zeta(a1 * np.cos(np.pi * (l + 0.5)/N) + a2, k, b))
        K = (l + 0.5)
        cheb = 2/N * np.matmul(f, np.cos(np.pi/N * np.outer(K, l)))
        alpha = np.pi/(m + 2)
        sa = np.sin(alpha)
        ca = np.cos(alpha)
        g_jackson = np.zeros(m + 1)
        for j in range(m + 1):
            g_jackson[j] = (1/sa)*((1-j/(m+2))*sa*np.cos(j*alpha) + (1/(m+2))*ca*np.sin(j*alpha))
   
        cl.append(cheb*g_jackson)
     
    return cl

# Approximated SGWT coefficients
def sgwt_cheby_sparse(f, L_sparse: sparse.csc_matrix, c, lmax, lmin):
    J = len(c)
    a1 = 0.5*(lmax - lmin)
    a2 = 0.5*(lmax + lmin)

    Twf_old = f
    Twf_cur = (sparse.csc_matrix.dot(L_sparse, f) - a2*f)/a1
    r = []
    for j in range(J):
        r.append(0.5*c[j][0]*Twf_old + c[j][1]*Twf_cur)
        
    maxM = len(c[0])
    for k in range(2, maxM):
        Twf_new = (2/a1)*(sparse.csc_matrix.dot(L_sparse, Twf_cur) - a2*Twf_cur) - Twf_old
        for j in range(J):
            r[j] = r[j] + c[j][k]*Twf_new
        Twf_old = Twf_cur
        Twf_cur = Twf_new
  
    return r

# Approximated SGWT inverse transform
def inv_tf_cheby_sparse(W_f_scales, L_sparse: sparse.csc_matrix, c, lmax, lmin):
    res = [sgwt_cheby_sparse(W_f_scales[k], L_sparse, [c[k]], lmax, lmin)[0] for k in range(len(c))]
    
    return np.array(res).sum(axis=0)


## SURE and thresholding
# Stein's unbiased risk estimate
def SURE(W_hat_f, W_tilde_f, n, t, beta, sigma, diagWWt):
    erisk = sum((W_hat_f - W_tilde_f)**2)
    dof = 2*sum((abs(W_tilde_f) >= t)*
                (1+(beta-1)*(t/abs(W_tilde_f))**beta)*
                diagWWt)
    SURE = -n*sigma**2 + erisk + dof*sigma**2

    return SURE

# Monte Carlo estimator of the SURE weights hat_gamma_{i,i}^2
def hat_diagWWt(L_sparse, lambda_max, cc, K=10, distrib="rademacher"):
    n = L_sparse.shape[0]
    J = len(cc) - 1
    hat_diagWWt_sum = np.zeros(n*(J+1))

    for _ in range(K):
        if distrib == "rademacher":
            epsilon = 2*np.random.binomial(1, 0.5, n) - 1
        elif distrib == "normal":
            epsilon = np.random.normal(0, 1, n)
        else:
            raise Warning('Allowable distributions are "rademacher" and "normal"')
        hat_W_epsilon = np.concatenate(sgwt_cheby_sparse(epsilon, L_sparse, cc, lambda_max, lmin=0))
        hat_diagWWt_sum += hat_W_epsilon**2

    hat_diagWWt = hat_diagWWt_sum/K
    
    return hat_diagWWt

# Thresholding function
def beta_threshold(y, t, beta):
    res = np.zeros_like(y, dtype=float)
    res[y != 0] = y[y != 0]*np.maximum(1 - (t/y[y != 0])**beta, 0)
    return res

# Level-dependent thresholding process minimizing MSE/l2-distance
def ld_MSE_threshold(W_tilde_f_scales, W_f_scales, beta, thresh):
    W_hat_f_scales = []

    for W_tilde_f_k, W_f_k in zip(W_tilde_f_scales, W_f_scales):
        if thresh == "all":
            thresholds = np.sort(np.unique(abs(W_tilde_f_k))) # Donoho and Johnstone's trick
        elif thresh == "percentiles":
            thresholds = [0.] + list(np.percentile(abs(W_tilde_f_k), range(101)))
        elif thresh == "deciles":
            thresholds = [0.] + list(np.percentile(abs(W_tilde_f_k), range(0, 101, 10)))
            
        W_hat_f_k_thresholds = [beta_threshold(np.array(W_tilde_f_k), t, beta) for t in thresholds]
        dist_thresholds = [np.linalg.norm(W_f_k - W_hat_f_k) for W_hat_f_k in W_hat_f_k_thresholds]
        W_hat_f_k = W_hat_f_k_thresholds[np.argmin(dist_thresholds)]
        
        W_hat_f_scales.append(W_hat_f_k)
        
    return W_hat_f_scales

# Level-dependent thresholding process minimizing SURE
def ld_SURE_threshold(W_tilde_f_scales, diagWWt, beta, sigma, policy, thresh):
    J = len(W_tilde_f_scales) - 1
    n = len(diagWWt)//(J+1)
    W_hat_f_scales = []

    for W_tilde_f_k, diagWWt_k in zip(W_tilde_f_scales, np.split(diagWWt, J+1)):
        if thresh == "all":
            thresholds = np.sort(np.unique(abs(W_tilde_f_k))) # Donoho and Johnstone's trick
        elif thresh == "percentiles":
            thresholds = [0.] + list(np.percentile(abs(W_tilde_f_k), range(101)))
        elif thresh == "deciles":
            thresholds = [0.] + list(np.percentile(abs(W_tilde_f_k), range(0, 101, 10)))
        # thresholds = np.sort(np.unique(abs(W_tilde_f_k)/np.sqrt(diagWWt_k)))
        
        W_hat_f_k_thresholds = []
        SURE_thresholds = []

        for t in thresholds:
            if policy == "dependent":
                t = t*np.sqrt(diagWWt_k)
            elif policy != "uniform":
                raise Warning('Allowable policies are "uniform" and "dependent"')
            
            W_hat_f_k = beta_threshold(np.array(W_tilde_f_k), t, beta)
            
            W_hat_f_k_thresholds.append(W_hat_f_k)
            SURE_thresholds.append(SURE(W_hat_f_k, W_tilde_f_k, n, t, beta, sigma, diagWWt_k))
            
        W_hat_f_scales.append(W_hat_f_k_thresholds[np.argmin(SURE_thresholds)])
        
    return W_hat_f_scales

# Signal-to-noise ratio
def snr(x, y):
    """
    Compute Signal to Noise Ratio.
    
       x : noise free signal
       y : denoised signal
    """
    # Turning list into numpy array
    if isinstance(x, list):
        x = np.array([coeff for scale in x for coeff in scale])
        y = np.array([coeff for scale in y for coeff in scale])
    
    z = 20 * np.log10(np.linalg.norm(x) / np.linalg.norm(x - y))
    return z