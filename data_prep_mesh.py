from scipy import sparse
import pandas as pd
import numpy as np
import SGWT

## Graph
# Data available at http://onk.msp-lab.org/projects/sgtf
tri = np.loadtxt('data/mesh/teapot_tri.txt', dtype=int)[:, 1:]
coord = np.loadtxt('data/mesh/teapot_coord.txt')*100
n_face = tri.shape[0]
n_node = coord.shape[0]

# Building adjacency matrix
A = np.zeros((n_node, n_node))
for face in np.sort(tri, axis=1):
    A[face[0], face[1]] = 1
    A[face[1], face[2]] = 1
    A[face[0], face[2]] = 1

A = A + A.T
D = np.diag(A.sum(axis=1))
L = D - A
L = L.astype(np.int64)
sparse.save_npz("data/mesh/L.npz", sparse.csc_matrix(L))

## Generating noisy signals for different sigma values
fx = coord[:, 0]
fy = coord[:, 2]
fz = coord[:, 1]
f = np.concatenate((fx, fy, fz))

sigma_grid = [20, 30, 40, 50]
n_sample = 10

snr_in_mean = []
snr_in_std = []
tilde_fx_sigma = []
tilde_fy_sigma = []
tilde_fz_sigma = []

for sigma in sigma_grid:
    tilde_fx_sample = np.empty((n_node, n_sample))
    tilde_fy_sample = np.empty((n_node, n_sample))
    tilde_fz_sample = np.empty((n_node, n_sample))
    snr_in_sigma = []
    
    for i in range(n_sample):
        np.random.seed(i)
        tilde_fx_sample[:, i] = fx + np.random.normal(0, sigma, n_node)
        tilde_fy_sample[:, i] = fy + np.random.normal(0, sigma, n_node)
        tilde_fz_sample[:, i] = fz + np.random.normal(0, sigma, n_node)
        tilde_f = np.concatenate((tilde_fx_sample[:, i], tilde_fy_sample[:, i], tilde_fz_sample[:, i]))
        snr_in_sigma.append(SGWT.snr(f, tilde_f))
    
    snr_in_mean.append(np.mean(snr_in_sigma))
    snr_in_std.append(np.std(snr_in_sigma))
    tilde_fx_sigma.append(tilde_fx_sample)
    tilde_fy_sigma.append(tilde_fy_sample)
    tilde_fz_sigma.append(tilde_fz_sample)

np.savez("data/mesh/tilde_fx_sigma.npz", *tilde_fx_sigma)
np.savez("data/mesh/tilde_fy_sigma.npz", *tilde_fy_sigma)
np.savez("data/mesh/tilde_fz_sigma.npz", *tilde_fz_sigma)

snr_in_sigma = pd.DataFrame({
    'sigma': sigma_grid,
    'snr_in_mean': snr_in_mean,
    'snr_in_std': snr_in_std,
})

snr_in_sigma.to_csv("data/mesh/snr_in_sigma.csv", index=False)