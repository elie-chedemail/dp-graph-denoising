from scipy import sparse
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import SGWT
import time

L_sparse = sparse.load_npz('data/NY/L.npz')
evalues, evectors = np.linalg.eigh(L_sparse.toarray())

with np.load('data/NY/pickups.npz') as data:
    f = data['pickups']

n = len(f)
sigma = 1
np.random.seed(0)
tilde_f = f + np.random.normal(0, sigma, n)

b = 2
beta = 2
lambda_max = np.max(evalues)
J = int(np.floor(np.log(lambda_max)/np.log(b))) + 2
tf = SGWT.frame(evalues, evectors, b=b)
W = np.array(tf)
diagWWt = (W**2).sum(axis=1) # = np.diag(W@W.T)

m = 100 # Chebyshev polynomial approximation degree
cc = SGWT.tight_sgwt_cheby(amin=0, amax=lambda_max, m=m, b=b, lmax=lambda_max)
ccj = SGWT.tight_sgwt_cheby_jackson(amin=0, amax=lambda_max, m=m, b=b, lmax=lambda_max)
W_f = np.concatenate(SGWT.sgwt_cheby_sparse(f, L_sparse, ccj, lambda_max, lmin=0))
W_tilde_f = np.concatenate(SGWT.sgwt_cheby_sparse(tilde_f, L_sparse, ccj, lambda_max, lmin=0))

thresholds = [0.] + list(np.percentile(abs(W_tilde_f), range(101))) # percentiles

MSE_thresholds = []
for t in thresholds:
    W_hat_f_t = SGWT.beta_threshold(np.array(W_tilde_f), t, beta)
    MSE_thresholds.append(np.linalg.norm(W_f - W_hat_f_t)**2)

t_star_MSE = thresholds[np.argmin(MSE_thresholds)]

W_hat_f = SGWT.beta_threshold(np.array(W_tilde_f), t_star_MSE, beta)
SURE = SGWT.SURE(W_hat_f, W_tilde_f, n, t_star_MSE, beta, sigma, diagWWt)

K_grid = range(1, 21) # sampling sizes
n_sampling = 50 # repetitions

MSE_gamma_mean_rade = []
MSE_gamma_mean_norm = []

SURE_mean_rade = []
SURE_mean_norm = []
SURE_CI_rade = []
SURE_CI_norm = []

start_time = time.time()
for K in K_grid:
    print('K = '+str(K))
    MSE_gamma_K_rade = []
    MSE_gamma_K_norm = []
    SURE_K_rade = []
    SURE_K_norm = []
    
    for _ in range(n_sampling):
        hat_diagWWt_rade = SGWT.hat_diagWWt(L_sparse, lambda_max, cc, K, "rademacher")
        hat_diagWWt_norm = SGWT.hat_diagWWt(L_sparse, lambda_max, cc, K, "normal")
        MSE_gamma_K_rade.append(((hat_diagWWt_rade - diagWWt)**2).mean())
        MSE_gamma_K_norm.append(((hat_diagWWt_norm - diagWWt)**2).mean())
        
        SURE_K_rade.append(SGWT.SURE(W_hat_f, W_tilde_f, n, t_star_MSE, beta, sigma, hat_diagWWt_rade))
        SURE_K_norm.append(SGWT.SURE(W_hat_f, W_tilde_f, n, t_star_MSE, beta, sigma, hat_diagWWt_norm))
        
    MSE_gamma_mean_rade.append(np.mean(MSE_gamma_K_rade))
    MSE_gamma_mean_norm.append(np.mean(MSE_gamma_K_norm))
    
    SURE_mean_rade.append(np.mean(SURE_K_rade))
    SURE_mean_norm.append(np.mean(SURE_K_norm))
    SURE_CI_rade.append(np.percentile(SURE_K_rade, [2.5, 97.5]))
    SURE_CI_norm.append(np.percentile(SURE_K_norm, [2.5, 97.5]))

print("--- %s seconds ---" % round(time.time() - start_time))

# Figure: Monte Carlo estimator of the weights gamma_{i,i}^2
plt.plot(K_grid, MSE_gamma_mean_rade, 'b^-', label='Rademacher')
plt.plot(K_grid, MSE_gamma_mean_norm, 'ro-', label='Gaussian')
plt.xticks([1, 5, 10, 15, 20])
plt.xlim((1, 20))
plt.xlabel('N')
plt.ylabel('MSE')
plt.legend()
plt.show()

# Figure: SURE Monte Carlo estimator
rade_line, = plt.plot(K_grid, SURE_mean_rade, 'b^-')
norm_line, = plt.plot(K_grid, SURE_mean_norm, 'ro-')
sure_line = plt.axhline(SURE, label='SURE', color='k')
plt.fill_between(K_grid, np.array(SURE_CI_rade)[:, 0], np.array(SURE_CI_rade)[:, 1], color='b', alpha=0.2)
plt.fill_between(K_grid, np.array(SURE_CI_norm)[:, 0], np.array(SURE_CI_norm)[:, 1], color='r', alpha=0.2)
plt.xticks([1, 5, 10, 15, 20])
plt.xlim((1, 20))
plt.xlabel('N')
plt.ylabel('SURE')
rade_patch = mpatches.Patch(color='b', alpha=0.2)
norm_patch = mpatches.Patch(color='r', alpha=0.2)
plt.legend([(rade_line, rade_patch), (norm_line, norm_patch), sure_line], ['Rademacher', 'Gaussian', 'SURE'])
plt.show()