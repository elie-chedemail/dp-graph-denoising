from dp_functions import sigma_cgm, sigma_agm
from shapely.geometry import Point
from scipy import sparse
import networkx as nx
import pandas as pd
import numpy as np
import osmnx as ox
import requests
import json
import SGWT

## Graph
OSM_ID = 'R3954665' # OpenStreetMap ID of Manhattan Island
manhattan_island_gdf = ox.geocode_to_gdf(OSM_ID, by_osmid=True)
G = ox.graph_from_polygon(manhattan_island_gdf.geometry[0], network_type='drive')
# fig, ax = ox.plot_graph(G, node_size=5, node_color='k', edge_color='gray', bgcolor='w')

# Converting to undirected MultiGraph and getting adjacency matrix
M = ox.utils_graph.get_undirected(G)
A = nx.adjacency_matrix(M).toarray()
A[A > 1] = 1 # removing parallel edges
A = A.astype(np.int64)
sparse.save_npz("data/NY/A.npz", sparse.csc_matrix(A))
D = np.diag(A.sum(axis=1))
L = D - A
sparse.save_npz("data/NY/L.npz", sparse.csc_matrix(L))


## Graph signal
# Downloading the 2014 Yellow Taxi Trip dataset from NYC Open Data
# and keeping taxi trips whose pickup is between 00:00 and 01:00 on Sept 24, 2014
json_file = "https://data.cityofnewyork.us/resource/gkne-dk5s.json" + \
            "?$where=pickup_datetime between '2014-09-24T00:00:00' and '2014-09-24T01:00:00'&$limit=20000"
json_data = requests.get(json_file)
text_data = json.loads(json_data.text)
taxi_df = pd.DataFrame(text_data)

taxi_df['pickup_longitude'] = pd.to_numeric(taxi_df['pickup_longitude'])
taxi_df['pickup_latitude'] = pd.to_numeric(taxi_df['pickup_latitude'])

# Keeping trips whose pickup is within Manhattan Island
taxi_df = taxi_df[[Point(x, y).within(manhattan_island_gdf['geometry'][0])
                   for x, y in zip(taxi_df['pickup_longitude'], taxi_df['pickup_latitude'])]]

# Projecting pickup locations to nearest intersection
pickup_nodes = ox.distance.nearest_nodes(G, X=taxi_df['pickup_longitude'], Y=taxi_df['pickup_latitude'])

# Signal: Number of taxi pickups for each node
pickups = [pickup_nodes.count(node) for node in list(G)]
np.savez("data/NY/pickups.npz", pickups=pickups)


## Generating noisy signals for different epsilon values
with np.load('data/NY/pickups.npz') as data:
    f = data['pickups']

n_node = len(f)
eps_grid = [0.2, 0.3, 0.5, 1] # privacy budgets 
delta = 10**-6
GS = 1 # global l2-sensitivity 
sigma_cgm_grid = [sigma_cgm(epsilon=eps, delta=delta, GS=GS) for eps in eps_grid]
sigma_agm_grid = [sigma_agm(epsilon=eps, delta=delta, GS=GS) for eps in eps_grid]
n_sample = 10

snr_in_cgm_mean = []
snr_in_agm_mean = []
snr_in_cgm_std = []
snr_in_agm_std = []
tilde_f_cgm_eps = []
tilde_f_agm_eps = []

for sig_cgm, sig_agm in zip(sigma_cgm_grid, sigma_agm_grid):
    tilde_f_cgm = np.empty((n_node, n_sample))
    tilde_f_agm = np.empty((n_node, n_sample))
    snr_in_cgm_list = []
    snr_in_agm_list = []
    
    for i in range(n_sample):
        np.random.seed(i)
        tilde_f_cgm[:, i] = f + np.random.normal(0, sig_cgm, n_node)
        np.random.seed(i)
        tilde_f_agm[:, i] = f + np.random.normal(0, sig_agm, n_node)
        snr_in_cgm_list.append(SGWT.snr(f, tilde_f_cgm[:, i]))
        snr_in_agm_list.append(SGWT.snr(f, tilde_f_agm[:, i]))
    
    snr_in_cgm_mean.append(np.mean(snr_in_cgm_list))
    snr_in_agm_mean.append(np.mean(snr_in_agm_list))
    snr_in_cgm_std.append(np.std(snr_in_cgm_list))
    snr_in_agm_std.append(np.std(snr_in_agm_list))
    
    tilde_f_cgm_eps.append(tilde_f_cgm)
    tilde_f_agm_eps.append(tilde_f_agm)
    
np.savez("data/NY/pickups_cgm_eps.npz", *tilde_f_cgm_eps)
np.savez("data/NY/pickups_agm_eps.npz", *tilde_f_agm_eps)

snr_in_eps = pd.DataFrame({
    'epsilon': eps_grid,
    'sigma_cgm': sigma_cgm_grid,
    'sigma_agm': sigma_agm_grid,
    'snr_in_cgm_mean': snr_in_cgm_mean,
    'snr_in_agm_mean': snr_in_agm_mean,
    'snr_in_cgm_std': snr_in_cgm_std,
    'snr_in_agm_std': snr_in_agm_std
})

snr_in_eps.to_csv("data/NY/snr_in_eps.csv", index=False)
