from dp_functions import sigma_cgm, sigma_agm
from shapely.geometry import Point
from scipy import sparse
import networkx as nx
import pandas as pd
import numpy as np
import osmnx as ox
import SGWT
import os
import re

## Graph
OSM_ID = 'R111968' # OpenStreetMap ID of San Francisco
sf_gdf = ox.geocode_to_gdf(OSM_ID, by_osmid=True)
G = ox.graph_from_polygon(sf_gdf.geometry[0], network_type='drive')
# fig, ax = ox.plot_graph(G, node_size=5, node_color='k', edge_color='gray', bgcolor='w')

# Converting to undirected MultiGraph and getting adjacency matrix
M = ox.utils_graph.get_undirected(G)
A = nx.adjacency_matrix(M).toarray()
A[A > 1] = 1 # removing parallel edges
A = A.astype(np.int64)
sparse.save_npz("data/SF/A.npz", sparse.csc_matrix(A))
D = np.diag(A.sum(axis=1))
L = D - A
sparse.save_npz("data/SF/L.npz", sparse.csc_matrix(L))


## Graph signal
# Data available at https://crawdad.org/epfl/mobility/20090224/cab/index.html (free registration required)

# Concatenating single taxi mobility traces into one table
directory = 'data/SF/cabspottingdata/'
names = ['latitude', 'longitude', 'occupancy', 'time']
location_df_list = []

for filename in os.listdir(directory):
    try:
        taxi_id = re.search('new_(.+?).txt', filename).group(1)
        taxi_df = pd.read_table(os.path.join(directory, filename), sep=' ', names=names)
        taxi_df['taxi_id'] = taxi_id
        taxi_df['time'] = pd.to_datetime(taxi_df['time'], unit='s')
        taxi_df = taxi_df.set_index('time')
        taxi_df = taxi_df.sort_index()
        location_df_list.append(taxi_df)
        
    except AttributeError:
        pass

location_df = pd.concat(location_df_list)

# Keeping taxi locations on May 25, 2008
location_df = location_df.loc['2008-05-25']

# Keeping taxi locations within San Francisco
location_df = location_df[[Point(x, y).within(sf_gdf['geometry'][0])
                           for x, y in zip(location_df['longitude'], location_df['latitude'])]]

# Projecting taxi locations to nearest intersection
location_df['nearest_node'] = ox.distance.nearest_nodes(G, X=location_df['longitude'],
                                                        Y=location_df['latitude'])

# Signal: Number of taxi pickups for each node
pickup_df_list = []
for taxi_id in location_df.taxi_id.unique():
    taxi_df = location_df[location_df['taxi_id'] == taxi_id]

    for i in range(1, len(taxi_df)):
        # Keeping pickups: occupied after vacant
        if np.array_equal(taxi_df.iloc[[i-1, i]].occupancy, [0, 1]):
            pickup_df_list.append(taxi_df.iloc[i])

pickup_df = pd.DataFrame(pickup_df_list)

pickups = np.zeros(G.number_of_nodes())
for node in pickup_df['nearest_node'].unique():
    pickups[np.array(list(G)) == node] = sum(pickup_df['nearest_node'] == node)

np.savez("data/SF/pickups.npz", pickups=pickups)


## Generating noisy signals for different epsilon values
with np.load('data/SF/pickups.npz') as data:
    f = data['pickups']

n_node = len(f)
eps_grid = [0.2, 0.5, 1] # privacy budgets
delta = 10**-6
GS = 2 # global l2-sensitivity
sigma_cgm_grid = [sigma_cgm(epsilon=eps, delta=delta, GS=GS) for eps in eps_grid]
sigma_agm_grid = [sigma_agm(epsilon=eps, delta=delta, GS=GS) for eps in eps_grid]
n_sample = 10

snr_in_cgm_mean = []
snr_in_agm_mean = []
snr_in_cgm_std = []
snr_in_agm_std = []
tilde_f_cgm_eps = []
tilde_f_agm_eps = []

for sig_cgm, sig_agm in zip(sigma_cgm_grid, sigma_agm_grid):
    tilde_f_cgm = np.empty((n_node, n_sample))
    tilde_f_agm = np.empty((n_node, n_sample))
    snr_in_cgm_list = []
    snr_in_agm_list = []
    
    for i in range(n_sample):
        np.random.seed(i)
        tilde_f_cgm[:, i] = f + np.random.normal(0, sig_cgm, n_node)
        np.random.seed(i)
        tilde_f_agm[:, i] = f + np.random.normal(0, sig_agm, n_node)
        snr_in_cgm_list.append(SGWT.snr(f, tilde_f_cgm[:, i]))
        snr_in_agm_list.append(SGWT.snr(f, tilde_f_agm[:, i]))
    
    snr_in_cgm_mean.append(np.mean(snr_in_cgm_list))
    snr_in_agm_mean.append(np.mean(snr_in_agm_list))
    snr_in_cgm_std.append(np.std(snr_in_cgm_list))
    snr_in_agm_std.append(np.std(snr_in_agm_list))
    
    tilde_f_cgm_eps.append(tilde_f_cgm)
    tilde_f_agm_eps.append(tilde_f_agm)
    
np.savez("data/SF/pickups_cgm_eps.npz", *tilde_f_cgm_eps)
np.savez("data/SF/pickups_agm_eps.npz", *tilde_f_agm_eps)

snr_in_eps = pd.DataFrame({
    'epsilon': eps_grid,
    'sigma_cgm': sigma_cgm_grid,
    'sigma_agm': sigma_agm_grid,
    'snr_in_cgm_mean': snr_in_cgm_mean,
    'snr_in_agm_mean': snr_in_agm_mean,
    'snr_in_cgm_std': snr_in_cgm_std,
    'snr_in_agm_std': snr_in_agm_std
})

snr_in_eps.to_csv("data/SF/snr_in_eps.csv", index=False)
