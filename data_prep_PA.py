from scipy import sparse
import pandas as pd
import numpy as np
import scipy.io
import SGWT

## Graph
# Data available at https://sparse.tamu.edu/SNAP/roadNet-PA
mat = scipy.io.loadmat('data/PA/roadNet-PA.mat')
A_sparse = mat['Problem'][0, 0][2]
A_sparse = A_sparse.astype(np.int64)
sparse.save_npz("data/PA/A.npz", A_sparse)
n = A_sparse.shape[0]

degrees = np.squeeze(np.asarray(A_sparse.sum(axis=1)))
D_sparse = sparse.diags(degrees)
L_sparse = D_sparse - A_sparse
L_sparse = L_sparse.astype(np.int64)
sparse.save_npz("data/PA/L.npz", L_sparse)


## Graph signal
p = 0.001
k = 4

np.random.seed(0)
x = np.random.binomial(1, p, n)
f = A_sparse**k@x
np.savez("data/PA/f.npz", f=f)


## Generating noisy signals for different sigma values
with np.load('data/PA/f.npz') as data:
    f = data['f']

n_node = len(f)
sigma_grid = np.linspace(0.5, 4, 8)
n_sample = 5

snr_in_mean = []
snr_in_std = []
tilde_f_sigma = []

for sigma in sigma_grid:
    tilde_f_sample = np.empty((n_node, n_sample))
    snr_in_sigma = []
    
    for i in range(n_sample):
        np.random.seed(i)
        tilde_f_sample[:, i] = f + np.random.normal(0, sigma, n_node)
        snr_in_sigma.append(SGWT.snr(f, tilde_f_sample[:, i]))
    
    snr_in_mean.append(np.mean(snr_in_sigma))
    snr_in_std.append(np.std(snr_in_sigma))
    tilde_f_sigma.append(tilde_f_sample)

np.savez("data/PA/tilde_f_sigma.npz", *tilde_f_sigma)

snr_in_sigma = pd.DataFrame({
    'sigma': sigma_grid,
    'snr_in_mean': snr_in_mean,
    'snr_in_std': snr_in_std,
})

snr_in_sigma.to_csv("data/PA/snr_in_sigma.csv", index=False)
