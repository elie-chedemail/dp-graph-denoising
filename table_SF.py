from texttable import Texttable
import pandas as pd
import latextable

snr_in_eps = pd.read_csv("data/SF/snr_in_eps.csv").round(2).astype(str)
snr_out_eps_sgwt_mse = pd.read_csv("data/SF/snr_out_eps_sgwt_mse.csv").round(2).astype(str)
snr_out_eps_sgwt_sure = pd.read_csv("data/SF/snr_out_eps_sgwt_sure.csv").round(2).astype(str)
snr_out_eps_sgwtcj_suremc = pd.read_csv("data/SF/snr_out_eps_sgwtcj_suremc.csv").round(2).astype(str)
snr_out_eps_dfsfusedlasso_mse = pd.read_csv("data/SF/snr_out_eps_dfsfusedlasso_mse.csv").round(2).astype(str)
snr_out_eps_dfsfusedlasso_sure = pd.read_csv("data/SF/snr_out_eps_dfsfusedlasso_sure.csv").round(2).astype(str)

# Table
table_cgm = pd.DataFrame(
    {
        "epsilon": snr_in_eps["epsilon"],
        "sigma": snr_in_eps["sigma_cgm"],
        "SNR_in": 
            snr_in_eps["snr_in_cgm_mean"] + " $\pm$ " + 
            snr_in_eps["snr_in_cgm_std"],
        "SGWT (MSE)": 
            snr_out_eps_sgwt_mse["snr_out_cgm_mean"] + " $\pm$ " + 
            snr_out_eps_sgwt_mse["snr_out_cgm_std"],
        "SGWT (SURE)": 
            snr_out_eps_sgwt_sure["snr_out_cgm_mean"] + " $\pm$ " + 
            snr_out_eps_sgwt_sure["snr_out_cgm_std"],
        "SGWT CJ (SURE MC)": 
            snr_out_eps_sgwtcj_suremc["snr_out_cgm_mean"] + " $\pm$ " + 
            snr_out_eps_sgwtcj_suremc["snr_out_cgm_std"],
        "DFS (MSE)": 
            snr_out_eps_dfsfusedlasso_mse["snr_out_cgm_mean"] + " $\pm$ " + 
            snr_out_eps_dfsfusedlasso_mse["snr_out_cgm_std"],
        "DFS (SURE)": 
            snr_out_eps_dfsfusedlasso_sure["snr_out_cgm_mean"] + " $\pm$ " + 
            snr_out_eps_dfsfusedlasso_sure["snr_out_cgm_std"]
    }
)

table_agm = pd.DataFrame(
    {
        "epsilon": snr_in_eps["epsilon"],
        "sigma": snr_in_eps["sigma_agm"],
        "SNR_in": 
            snr_in_eps["snr_in_agm_mean"] + " $\pm$ " + 
            snr_in_eps["snr_in_agm_std"],
        "SGWT (MSE)": 
            snr_out_eps_sgwt_mse["snr_out_agm_mean"] + " $\pm$ " + 
            snr_out_eps_sgwt_mse["snr_out_agm_std"],
        "SGWT (SURE)": 
            snr_out_eps_sgwt_sure["snr_out_agm_mean"] + " $\pm$ " + 
            snr_out_eps_sgwt_sure["snr_out_agm_std"],
        "SGWT CJ (SURE MC)": 
            snr_out_eps_sgwtcj_suremc["snr_out_agm_mean"] + " $\pm$ " + 
            snr_out_eps_sgwtcj_suremc["snr_out_agm_std"],
        "DFS (MSE)": 
            snr_out_eps_dfsfusedlasso_mse["snr_out_agm_mean"] + " $\pm$ " + 
            snr_out_eps_dfsfusedlasso_mse["snr_out_agm_std"],
        "DFS (SURE)": 
            snr_out_eps_dfsfusedlasso_sure["snr_out_agm_mean"] + " $\pm$ " + 
            snr_out_eps_dfsfusedlasso_sure["snr_out_agm_std"]
    }
)

table = table_agm.T
rows = table.reset_index().to_numpy()

# Latex
table_latex = Texttable()
table_latex.set_precision(2)
table_latex.set_cols_align(["l"] + ["c"]*table.shape[1])
table_latex.set_deco(False)
table_latex.add_rows(rows, header=False)

print(table_latex.draw())
print(latextable.draw_latex(table_latex))