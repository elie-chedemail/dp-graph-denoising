import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse
from SGWT import zeta

L_sparse = sparse.load_npz('data/NY/L.npz')
evalues, evectors = np.linalg.eigh(L_sparse.toarray())
b = 2
J = int(np.floor(np.log(max(evalues))/np.log(b))) + 2

x = np.linspace(0, max(evalues), 500)
zeta_scales = [np.sqrt(zeta(x, k, b)) for k in range(J+1)]

# Figure: Finite partition of unity
plt.plot(x, zeta_scales[0], '-', label=r'$\psi_0$')
plt.plot(x, zeta_scales[1], '--', label=r'$\psi_1$')
plt.plot(x, zeta_scales[2], '-.', label=r'$\psi_2$')
plt.plot(x, zeta_scales[3], linestyle=(0, (6, 2, 1, 2, 1, 2)), label=r'$\psi_3$')
plt.plot(x, zeta_scales[4], ':', label=r'$\psi_4$')

plt.xlim((0, max(evalues)))
plt.xlabel(r'$x$')
plt.ylabel(r'$\psi_j(x)$')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.grid(ls=':')
plt.tight_layout()
plt.show()
