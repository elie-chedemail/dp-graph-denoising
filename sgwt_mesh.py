from sklearn.decomposition import TruncatedSVD
from plotly.subplots import make_subplots
from scipy import sparse
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import SGWT
import time

coord = np.loadtxt('data/mesh/teapot_coord.txt')*100
fx = coord[:, 0]
fy = coord[:, 2]
fz = coord[:, 1]
f = np.concatenate((fx, fy, fz))
n_node = coord.shape[0]

## Denoising
L_sparse = sparse.load_npz('data/mesh/L.npz')
b = 2
beta = 2
policy = "uniform"
thresh = "percentiles" # threshold set in abs(W_f)

# Complete SGWT
evalues, evectors = np.linalg.eigh(L_sparse.toarray())
J = int(np.floor(np.log(max(evalues))/np.log(b))) + 2
tf = SGWT.frame(evalues, evectors, b=b)
W = np.array(tf)
W_fx_scales = np.split(W@fx, J+1)
W_fy_scales = np.split(W@fy, J+1)
W_fz_scales = np.split(W@fz, J+1)

# SGWT polynomial approximation
svd = TruncatedSVD(n_components=1, n_iter=50, random_state=0)
svd.fit(L_sparse)
lambda_max = svd.singular_values_[0] # maximum eigenvalue estimate
m = 100 # Chebyshev polynomial approximation degree
cc = SGWT.tight_sgwt_cheby(amin=0, amax=lambda_max, m=m, lmax=lambda_max, b=b)
ccj = SGWT.tight_sgwt_cheby_jackson(amin=0, amax=lambda_max, m=m, lmax=lambda_max, b=b)
hat_diagWWt = SGWT.hat_diagWWt(L_sparse, lambda_max, cc, K=10)

tilde_fx_sigma = np.load("data/mesh/tilde_fx_sigma.npz")
tilde_fy_sigma = np.load("data/mesh/tilde_fy_sigma.npz")
tilde_fz_sigma = np.load("data/mesh/tilde_fz_sigma.npz")
snr_in_sigma = pd.read_csv("data/mesh/snr_in_sigma.csv")

snr_out_mean_sgwt_mse = []
snr_out_mean_sgwtcj_suremc = []
snr_out_std_sgwt_mse = []
snr_out_std_sgwtcj_suremc = []

t1 = time.time() # ~ 1 minute
for sig_id, sig in enumerate(tilde_fx_sigma.files):
    print(str(sig_id+1) + "/" + str(snr_in_sigma.shape[0]))
    snr_out_sgwt_mse = []
    snr_out_sgwtcj_suremc = []
    
    tilde_fx_sample = tilde_fx_sigma[sig]
    tilde_fy_sample = tilde_fy_sigma[sig]
    tilde_fz_sample = tilde_fz_sigma[sig]
    
    for i in range(tilde_fx_sample.shape[1]):
        tilde_fx = tilde_fx_sample[:, i]
        tilde_fy = tilde_fy_sample[:, i]
        tilde_fz = tilde_fz_sample[:, i]
        
        # SGWT (MSE)
        W_tilde_fx_scales = np.split(W@tilde_fx, J+1)
        W_tilde_fy_scales = np.split(W@tilde_fy, J+1)
        W_tilde_fz_scales = np.split(W@tilde_fz, J+1)
        W_hat_fx_scales = SGWT.ld_MSE_threshold(W_tilde_fx_scales, W_fx_scales, beta, thresh)
        W_hat_fy_scales = SGWT.ld_MSE_threshold(W_tilde_fy_scales, W_fy_scales, beta, thresh)
        W_hat_fz_scales = SGWT.ld_MSE_threshold(W_tilde_fz_scales, W_fz_scales, beta, thresh)
        hat_fx_sgwt_mse = W.T@np.concatenate(W_hat_fx_scales)
        hat_fy_sgwt_mse = W.T@np.concatenate(W_hat_fy_scales)
        hat_fz_sgwt_mse = W.T@np.concatenate(W_hat_fz_scales)
        hat_f_sgwt_mse = np.concatenate((hat_fx_sgwt_mse, hat_fy_sgwt_mse, hat_fz_sgwt_mse))
        
        # SGWT Chebyshev-Jackson (SURE Monte Carlo)
        sigma = snr_in_sigma.loc[sig_id, 'sigma']
        W_tilde_fx_scales = SGWT.sgwt_cheby_sparse(tilde_fx, L_sparse, ccj, lambda_max, lmin=0)
        W_tilde_fy_scales = SGWT.sgwt_cheby_sparse(tilde_fy, L_sparse, ccj, lambda_max, lmin=0)
        W_tilde_fz_scales = SGWT.sgwt_cheby_sparse(tilde_fz, L_sparse, ccj, lambda_max, lmin=0)
        W_hat_fx_scales = SGWT.ld_SURE_threshold(W_tilde_fx_scales, hat_diagWWt, beta, sigma, policy, thresh)
        W_hat_fy_scales = SGWT.ld_SURE_threshold(W_tilde_fy_scales, hat_diagWWt, beta, sigma, policy, thresh)
        W_hat_fz_scales = SGWT.ld_SURE_threshold(W_tilde_fz_scales, hat_diagWWt, beta, sigma, policy, thresh)
        hat_fx_sgwtcj_suremc = SGWT.inv_tf_cheby_sparse(W_hat_fx_scales, L_sparse, ccj, lambda_max, lmin=0)
        hat_fy_sgwtcj_suremc = SGWT.inv_tf_cheby_sparse(W_hat_fy_scales, L_sparse, ccj, lambda_max, lmin=0)
        hat_fz_sgwtcj_suremc = SGWT.inv_tf_cheby_sparse(W_hat_fz_scales, L_sparse, ccj, lambda_max, lmin=0)
        hat_f_sgwtcj_suremc = np.concatenate((hat_fx_sgwtcj_suremc, hat_fy_sgwtcj_suremc, hat_fz_sgwtcj_suremc))
        
        snr_out_sgwt_mse.append(SGWT.snr(f, hat_f_sgwt_mse))
        snr_out_sgwtcj_suremc.append(SGWT.snr(f, hat_f_sgwtcj_suremc))
        
    snr_out_mean_sgwt_mse.append(np.mean(snr_out_sgwt_mse))
    snr_out_mean_sgwtcj_suremc.append(np.mean(snr_out_sgwtcj_suremc))
    snr_out_std_sgwt_mse.append(np.std(snr_out_sgwt_mse))
    snr_out_std_sgwtcj_suremc.append(np.std(snr_out_sgwtcj_suremc))

snr_out_sigma_sgwt_mse = pd.DataFrame({
    'snr_out_mean': snr_out_mean_sgwt_mse,
    'snr_out_std': snr_out_std_sgwt_mse,
})
snr_out_sigma_sgwtcj_suremc = pd.DataFrame({
    'snr_out_mean': snr_out_mean_sgwtcj_suremc,
    'snr_out_std': snr_out_std_sgwtcj_suremc,
})
print("--- %s seconds ---" % round(time.time() - t1))

snr_out_sigma_sgwt_mse.to_csv("data/mesh/snr_out_sigma_sgwt_mse.csv", index=False)
snr_out_sigma_sgwtcj_suremc.to_csv("data/mesh/snr_out_sigma_sgwtcj_suremc.csv", index=False)


## Visualizing results (SNR)
snr_in_sigma = pd.read_csv("data/mesh/snr_in_sigma.csv")
snr_out_sigma_sgwt_mse = pd.read_csv("data/mesh/snr_out_sigma_sgwt_mse.csv")
snr_out_sigma_sgwtcj_suremc = pd.read_csv("data/mesh/snr_out_sigma_sgwtcj_suremc.csv")

plt.plot(snr_in_sigma['sigma'], snr_in_sigma['snr_in_mean'], 'k-', label='Input noise')
plt.plot(snr_in_sigma['sigma'], snr_out_sigma_sgwt_mse['snr_out_mean'], 'c--', label='SGWT (MSE)')
plt.plot(snr_in_sigma['sigma'], snr_out_sigma_sgwtcj_suremc['snr_out_mean'], 'm:', label='SGWT CJ (SURE MC)')

plt.xlim((snr_in_sigma['sigma'].min(), snr_in_sigma['sigma'].max()))
plt.xlabel(r'$\sigma$')
plt.ylabel('SNR (dB)')
plt.legend()
plt.grid(ls=':')
plt.show()


## Visualizing results (3D mesh, on a given realization)
tri = np.loadtxt('data/mesh/teapot_tri.txt', dtype=int)[:, 1:]
hat_f_sgwt_mse = np.load("data/mesh/hat_f_sgwt_mse.npz")
hat_f_sgwtcj_suremc = np.load("data/mesh/hat_f_sgwtcj_suremc.npz")
hat_fx_sgwt_mse = hat_f_sgwt_mse['x']
hat_fy_sgwt_mse = hat_f_sgwt_mse['y']
hat_fz_sgwt_mse = hat_f_sgwt_mse['z']
hat_fx_sgwtcj_suremc = hat_f_sgwtcj_suremc['x']
hat_fy_sgwtcj_suremc = hat_f_sgwtcj_suremc['y']
hat_fz_sgwtcj_suremc = hat_f_sgwtcj_suremc['z']


fig = make_subplots(
    rows=2, cols=2,
    subplot_titles=("Original 3D mesh", "Noisy 3D mesh (σ = 20)", "SGWT (MSE)", "SGWT CJ (SURE MC)"),
    specs=[[{'type': 'scene'}, {'type': 'scene'}], [{'type': 'scene'}, {'type': 'scene'}]],
    horizontal_spacing=0.05, vertical_spacing=0.1)

fig.add_trace(
    go.Mesh3d(
        x=fx,
        y=fy,
        z=fz,
        colorscale=[[0, 'gold'],
                    [0.5, 'mediumturquoise'],
                    [1, 'magenta']],
        intensity=np.linspace(0, 1, n_node, endpoint=True),
        i=tri[:, 0],
        j=tri[:, 1],
        k=tri[:, 2],
        name='y',
        showscale=True),
    row=1, col=1
)

fig.add_trace(
    go.Mesh3d(
        x=tilde_fx_sigma['arr_0'][:, 0],
        y=tilde_fy_sigma['arr_0'][:, 0],
        z=tilde_fz_sigma['arr_0'][:, 0],
        colorscale=[[0, 'gold'],
                    [0.5, 'mediumturquoise'],
                    [1, 'magenta']],
        intensity=np.linspace(0, 1, n_node, endpoint=True),
        i=tri[:, 0],
        j=tri[:, 1],
        k=tri[:, 2],
        name='y',
        showscale=True),
    row=1, col=2
)

fig.add_trace(
    go.Mesh3d(
        x=hat_fx_sgwt_mse,
        y=hat_fy_sgwt_mse,
        z=hat_fz_sgwt_mse,
        colorscale=[[0, 'gold'],
                    [0.5, 'mediumturquoise'],
                    [1, 'magenta']],
        intensity=np.linspace(0, 1, n_node, endpoint=True),
        i=tri[:, 0],
        j=tri[:, 1],
        k=tri[:, 2],
        name='y',
        showscale=True),
    row=2, col=1
)

fig.add_trace(
    go.Mesh3d(
        x=hat_fx_sgwtcj_suremc,
        y=hat_fy_sgwtcj_suremc,
        z=hat_fz_sgwtcj_suremc,
        colorscale=[[0, 'gold'],
                    [0.5, 'mediumturquoise'],
                    [1, 'magenta']],
        intensity=np.linspace(0, 1, n_node, endpoint=True),
        i=tri[:, 0],
        j=tri[:, 1],
        k=tri[:, 2],
        name='y',
        showscale=True),
    row=2, col=2
)

fig.show()