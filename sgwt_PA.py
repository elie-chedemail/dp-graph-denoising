from sklearn.decomposition import TruncatedSVD
from scipy import sparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import SGWT
import time

with np.load('data/PA/f.npz') as data:
    f = data['f']

## Denoising
L_sparse = sparse.load_npz('data/PA/L.npz')
b = 2
beta = 2
policy = "uniform"
thresh = "percentiles" # threshold set in abs(W_f)

# SGWT polynomial approximation
svd = TruncatedSVD(n_components=1, n_iter=20, random_state=0)
svd.fit(L_sparse)
lambda_max = svd.singular_values_[0] # maximum eigenvalue estimate
J = int(np.floor(np.log(lambda_max)/np.log(b))) + 2
m = 100 # Chebyshev polynomial approximation degree
cc = SGWT.tight_sgwt_cheby(amin=0, amax=lambda_max, m=m, lmax=lambda_max, b=b)
ccj = SGWT.tight_sgwt_cheby_jackson(amin=0, amax=lambda_max, m=m, lmax=lambda_max, b=b)
W_f_scales = SGWT.sgwt_cheby_sparse(f, L_sparse, ccj, lambda_max, lmin=0)
hat_diagWWt = SGWT.hat_diagWWt(L_sparse, lambda_max, cc, K=10) # 1 minute

tilde_f_sigma = np.load("data/PA/tilde_f_sigma.npz")
snr_in_sigma = pd.read_csv("data/PA/snr_in_sigma.csv")

snr_out_mean_sgwtcj_mse = []
snr_out_mean_sgwtcj_suremc = []
snr_out_std_sgwtcj_mse = []
snr_out_std_sgwtcj_suremc = []

t1 = time.time() # ~ 2.5 hours
for sig_id, sig in enumerate(tilde_f_sigma.files):
    t2 = time.time()
    print(str(sig_id+1) + "/" + str(snr_in_sigma.shape[0]))
    snr_out_sgwtcj_mse = []
    snr_out_sgwtcj_suremc = []
    
    tilde_f_sample = tilde_f_sigma[sig]

    for i in range(tilde_f_sample.shape[1]):
        tilde_f = tilde_f_sample[:, i]
        
        # SGWT Chebyshev-Jackson (MSE)
        t3 = time.time()
        W_tilde_f_scales = SGWT.sgwt_cheby_sparse(tilde_f, L_sparse, ccj, lambda_max, lmin=0)
        W_hat_f_scales = SGWT.ld_MSE_threshold(W_tilde_f_scales, W_f_scales, beta, thresh)
        hat_f_sgwtcj_mse = SGWT.inv_tf_cheby_sparse(W_hat_f_scales, L_sparse, ccj, lambda_max, lmin=0)
        print("--- SGWT CJ (MSE): %s seconds ---" % round(time.time() - t3))
        
        # SGWT Chebyshev-Jackson (SURE Monte Carlo)
        t4 = time.time()
        sigma = snr_in_sigma.loc[sig_id, 'sigma']
        W_hat_f_scales = SGWT.ld_SURE_threshold(W_tilde_f_scales, hat_diagWWt, beta, sigma, policy, thresh)
        hat_f_sgwtcj_suremc = SGWT.inv_tf_cheby_sparse(W_hat_f_scales, L_sparse, ccj, lambda_max, lmin=0)
        print("--- SGWT CJ (SURE MC): %s seconds ---" % round(time.time() - t4))
        
        snr_out_sgwtcj_mse.append(SGWT.snr(f, hat_f_sgwtcj_mse))
        snr_out_sgwtcj_suremc.append(SGWT.snr(f, hat_f_sgwtcj_suremc))
    
    snr_out_mean_sgwtcj_mse.append(np.mean(snr_out_sgwtcj_mse))
    snr_out_mean_sgwtcj_suremc.append(np.mean(snr_out_sgwtcj_suremc))
    snr_out_std_sgwtcj_mse.append(np.std(snr_out_sgwtcj_mse))
    snr_out_std_sgwtcj_suremc.append(np.std(snr_out_sgwtcj_suremc))
    
    print("--- %s seconds ---" % round(time.time() - t2))

snr_out_sigma_sgwtcj_mse = pd.DataFrame({
    'snr_out_mean': snr_out_mean_sgwtcj_mse,
    'snr_out_std': snr_out_std_sgwtcj_mse,
})
snr_out_sigma_sgwtcj_suremc = pd.DataFrame({
    'snr_out_mean': snr_out_mean_sgwtcj_suremc,
    'snr_out_std': snr_out_std_sgwtcj_suremc,
})
print("--- %s seconds ---" % round(time.time() - t1))

snr_out_sigma_sgwtcj_mse.to_csv("data/PA/snr_out_sigma_sgwtcj_mse.csv", index=False)
snr_out_sigma_sgwtcj_suremc.to_csv("data/PA/snr_out_sigma_sgwtcj_suremc.csv", index=False)


## Visualizing results (Figure PA)
snr_in_sigma = pd.read_csv("data/PA/snr_in_sigma.csv")
# snr_out_sigma_sgwtcj_mse = pd.read_csv("data/PA/snr_out_sigma_sgwtcj_mse.csv")
snr_out_sigma_sgwtcj_suremc = pd.read_csv("data/PA/snr_out_sigma_sgwtcj_suremc.csv")
snr_out_sigma_dfsfusedlasso_mse = pd.read_csv("data/PA/snr_out_sigma_dfsfusedlasso_mse.csv")
# snr_out_sigma_dfsfusedlasso_sure = pd.read_csv("data/PA/snr_out_sigma_dfsfusedlasso_sure.csv")

plt.plot(snr_in_sigma['sigma'], snr_in_sigma['snr_in_mean'], 'k-', label='Input noise')
plt.plot(snr_in_sigma['sigma'], snr_out_sigma_dfsfusedlasso_mse['snr_out_mean'], 'ro-', label='DFS fused lasso (MSE)')
# plt.plot(snr_in_sigma['sigma'], snr_out_sigma_dfsfusedlasso_sure['snr_out_mean'], 'yP-', label='DFS fused lasso (SURE)')
# plt.plot(snr_in_sigma['sigma'], snr_out_sigma_sgwtcj_mse['snr_out_mean'], 'ms-', label='SGWT CJ (MSE)')
plt.plot(snr_in_sigma['sigma'], snr_out_sigma_sgwtcj_suremc['snr_out_mean'], 'b^-', label='SGWT CJ (SURE MC)')

plt.xlim((snr_in_sigma['sigma'].min(), snr_in_sigma['sigma'].max()))
plt.xlabel(r'$\sigma$')
plt.ylabel('SNR (dB)')
plt.legend()
plt.grid(ls=':')
plt.show()