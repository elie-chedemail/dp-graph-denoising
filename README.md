# Large Graph Signal Denoising with Application to Differential Privacy

The scripts in this repository implement an extension of Stein's unbiased risk estimate (SURE) to large-scale graphs in the context of signal denoising with the spectral graph wavelet transform (SGWT) from Hammond, Vandergheynst, and Gribonval (2011). This file provides instructions to reproduce the experiments presented in Chedemail et al. (2022).

## Installation

All graph matrices, original signals and experimental results needed to reproduce the figures and tables are stored in the `data` folder. If you want to rerun the experiments, follow these preliminary steps.

- The construction of the graphs of New York City and San Francisco requires the `osmnx` Python package. Installation instructions are provided [here](https://osmnx.readthedocs.io/en/stable/#installation).

- The San Francisco graph signal is built from a dataset available [here](https://crawdad.org/epfl/mobility/20090224/cab/index.html). A free registration is required before downloading.

## Reproduction of figures and tables

### $\omega$ and $h_c$ functions

Run `omega_vis.py` to reproduce Figure 1.

|Figure 1|
|:---:|
![](/README_files/Figure_1.png)

### Finite partition of unity

Run `psi_j_vis.py` to reproduce Figure 2.

|Figure 2|
|:---:|
![](/README_files/Figure_2.png)

### Monte Carlo estimator of the weights $\gamma_{i,i}^2$ and SURE Monte Carlo estimator

Run `sure_mc_vis.py` to reproduce Figure 3 and Figure 4.

Figure 3  |  Figure 4
:---:|:---:
![](/README_files/Figure_3.png)  |  ![](/README_files/Figure_4.png)

### Denoising of differentially private small graph signals

Run `table_NY.py` and `table_SF.py` to reproduce Table I and Table II associated with the New York (NY) and San Francisco (SF) datasets, respectively.

For each of them, associated experiments can be reproduced by running the following scripts:

- `data_prep_*.py` generates the graph matrices, original and noisy graph signals
- `dfs_fusedlasso_*.py` applies the DFS fused lasso method from Padilla et al. (2018)
- `sgwt_*.py` applies the SGWT with SURE and their respective approximations

### Denoising of large graph signals

Run `figure_PA.py` to reproduce Figure 5 associated with the Pennsylvania (PA) graph.

|Figure 5|
|:---:|
![](/README_files/Figure_5.png)

Experiments on the large graph can be reproduced by running `data_prep_PA.py`, `dfs_fusedlasso_PA.py` and `sgwt_PA.py`.

## Supplementary experiment: 3D mesh denoising

This experiment is directly inspired from Onuki et al. (2016) in which the complete SGWT is used to remove noise from a 3D mesh of the Utah teapot. Here, we compare it with our proposed method. 

Run `data_prep_mesh.py` and `sgwt_mesh.py` to reproduce the experiment or `table_mesh.py` to directly get the results.

## References

E. Chedemail, B. de Loynes, F. Navarro and B. Olivier, "Large Graph Signal Denoising With Application to Differential Privacy," in IEEE Transactions on Signal and Information Processing over Networks, vol. 8, pp. 788-798, 2022, doi: 10.1109/TSIPN.2022.3205555.

D. K. Hammond, P. Vandergheynst, and R. Gribonval. Wavelets on graphs via spectral graph theory. Applied and Computational Harmonic Analysis, 30(2):129–150, 2011.

O. Hernan Madrid Padilla, J. Sharpnack, J. G. Scott, and R. J. Tibshirani. The DFS Fused Lasso: Linear-Time Denoising over General Graphs. Journal of Machine Learning Research, 18(176):1–36, 2018.

M. Onuki, S. Ono, M. Yamagishi, and Y. Tanaka. Graph signal denoising via trilateral filter on graph spectral domain. IEEE Transactions on Signal and Information Processing over Networks, 2(2):137–148, 2016.
