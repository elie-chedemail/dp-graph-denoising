import matplotlib.pyplot as plt
import pandas as pd

snr_in_sigma = pd.read_csv("data/PA/snr_in_sigma.csv")
# snr_out_sigma_sgwtcj_mse = pd.read_csv("data/PA/snr_out_sigma_sgwtcj_mse.csv")
snr_out_sigma_sgwtcj_suremc = pd.read_csv("data/PA/snr_out_sigma_sgwtcj_suremc.csv")
snr_out_sigma_dfsfusedlasso_mse = pd.read_csv("data/PA/snr_out_sigma_dfsfusedlasso_mse.csv")
# snr_out_sigma_dfsfusedlasso_sure = pd.read_csv("data/PA/snr_out_sigma_dfsfusedlasso_sure.csv")

plt.plot(snr_in_sigma['sigma'], snr_in_sigma['snr_in_mean'], 'k-', label='Input noise')
plt.plot(snr_in_sigma['sigma'], snr_out_sigma_dfsfusedlasso_mse['snr_out_mean'], 'ro-', label='DFS fused lasso (MSE)')
# plt.plot(snr_in_sigma['sigma'], snr_out_sigma_dfsfusedlasso_sure['snr_out_mean'], 'yP-', label='DFS fused lasso (SURE)')
# plt.plot(snr_in_sigma['sigma'], snr_out_sigma_sgwtcj_mse['snr_out_mean'], 'ms-', label='SGWT CJ (MSE)')
plt.plot(snr_in_sigma['sigma'], snr_out_sigma_sgwtcj_suremc['snr_out_mean'], 'b^-', label='SGWT CJ (SURE MC)')

plt.xlim((snr_in_sigma['sigma'].min(), snr_in_sigma['sigma'].max()))
plt.xlabel(r'$\sigma$')
plt.ylabel('SNR (dB)')
plt.legend()
plt.grid(ls=':')
plt.show()